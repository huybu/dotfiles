# dotfiles

The place I store my lazily made i3wm dotfiles

<img src="img/screenshot.png" width="600">

## How to install?

- **Note**: This have only been tested on Arch Linux 

1. First, clone this repo

2. Then, install the following package (AUR packages included): `i3 polybar-git picom xwallpaper rofi hideIt.sh-git noto-fonts ttf-noto-nerd kitty`

3. Clone [this](https://github.com/adi1090x/rofi) rofi config repo and install it

4. Then copy `.config` directory inside the repo into your home directory

- **Note**: Doing so will replace your current config for i3, polybar and picom so be sure to back them up

5. Now, log out, log back into i3 and enjoy

## Other thing to Note

- `kitty` is optional, edit `.config/i3/config` and replace `kitty` (at line 20) with your favourite terminal emulator

- To change the screen's brightness using the backlight module you need to install `light` make the user in group `video`

- i use arch btw ;)
