#!/bin/bash

if which polybar; then
  pkill polybar
  polybar -r &
  if which hideIt.sh; then
    kill $(pgrep -f $(which hideIt.sh))
    sleep 1
    hideIt.sh --class "polybar" -w -r 0x768+1366+-35 -d bottom -i 0 &
  fi
fi
